FROM mcr.microsoft.com/mssql/server:2019-latest

USER root

COPY migrations migrations
COPY seeds seeds
COPY knock_knock.sql knock_knock.sql
COPY import-data.sh import-data.sh
COPY entrypoint.sh entrypoint.sh

RUN chmod +x import-data.sh

ENTRYPOINT /bin/bash ./entrypoint.sh