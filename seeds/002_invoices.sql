USE Shop;

DECLARE @aID INTEGER = (select ID
                   from clients
                   where LastName = 'Lee');
DECLARE @bID int = (select ID
                   from clients
                   where LastName = 'White');

INSERT INTO Invoices(Amount, ClientID)
Values (200, @aID),
       (123, @bID),
       (234, @bID)
