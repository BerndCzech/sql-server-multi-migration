for i in {1..50}; do
  /opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P "$SA_PASSWORD" -d master -i knock_knock.sql
  if [ $? -eq 0 ]; then
    for filename in $(find ./migrations -name '*.sql' -maxdepth 3 | sort); do
      echo "migrating $filename"
      /opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P "$SA_PASSWORD" -d master -i "$filename"
    done
    echo "migrations done"
    for filename in $(find ./seeds -name '*.sql' -maxdepth 3 | sort); do
      echo "seeding $filename"
      /opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P "$SA_PASSWORD" -d master -i "$filename"
    done
    echo "seeds done"
    break
  else
    echo "not ready yet..."
    sleep 0.5
  fi
done
