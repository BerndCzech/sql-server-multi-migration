[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

# MS SQL Server 2019 Multi-Migration

The project contains everything to start an MS SQL Server with docker-compose. Moreover, it initializes with several
migration and seed files. If the *.sql files in [migrations](migrations) and [seeds](seeds) have some alphanumeric order
they will be executed in that order, e.g. timestamp_script.sql or 001.sql ... 003.sql

## Usage

### Start

`docker-compose up`

### Stop

`docker-compose down`

### (Clean)

`"y" | docker volume prune`

## Contributing

Please file an issue first. After we agree on a patch, you are free to send a pull request.

## License

Licensed under [MIT License](LICENSE).

