USE Shop;
CREATE TABLE Invoices
(
    ID       INT IDENTITY (1,1) PRIMARY KEY,
    Amount   MONEY NOT NULL,
    ClientID INT FOREIGN KEY REFERENCES Clients (ID) NOT NULL
)

