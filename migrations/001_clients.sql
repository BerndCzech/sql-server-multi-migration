USE Shop;

CREATE TABLE clients
(
    ID        INT IDENTITY (1,1) PRIMARY KEY,
    FirstName NVARCHAR(max) NOT NULL,
    LastName  NVARCHAR(max) NOT NULL,
    Active    BIT DEFAULT (1) NOT NULL
);